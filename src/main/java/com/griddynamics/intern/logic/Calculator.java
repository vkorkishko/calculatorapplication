package com.griddynamics.intern.logic;

import com.griddynamics.intern.interfaces.DoubleArgumentFunction;
import com.griddynamics.intern.interfaces.OneArgumentFunction;

import java.util.*;

public class Calculator {
    private final String OPERATORS = "+-*/^%";
    private final String[] FUNCTIONS = {"sin", "cos", "tan", "sqrt", "abs"};
    private final String SEPARATOR = ",";
    private String[] operations = OPERATORS.split("");
    private Stack<String> stringStack = new Stack<>();
    private StringBuilder resultString = null;

    private Map<String, OneArgumentFunction> oneArgumentFunctionMap = new HashMap<>() {{
        put("sin", Math::sin);
        put("cos", Math::cos);
        put("tang", Math::tan);
        put("sqrt", Math::sqrt);
        put("abs", Math::abs);
    }};
    private Map<String, DoubleArgumentFunction> doubleArgumentFunctionMap = new HashMap<>() {{
        put("+", (firstValue, secondValue) -> firstValue + secondValue);
        put("-", (firstValue, secondValue) -> firstValue - secondValue);
        put("/", (firstValue, secondValue) -> firstValue / secondValue);
        put("*", (firstValue, secondValue) -> firstValue * secondValue);
        put("%", (firstValue, secondValue) -> firstValue % secondValue);
        put("^", Math::pow);
    }};

    private Map<String, Integer> operationMap = new HashMap<>() {{
        put("^", 3);
        put("/", 2);
        put("*", 2);
        put("%", 2);
        put("+", 1);
        put("-", 1);
    }};


    public String convertString(String inputString) throws Exception {

        resultString = new StringBuilder();

        StringTokenizer stringTokenizer = new StringTokenizer(inputString, OPERATORS + SEPARATOR + " ()", true);

        fillStack(stringTokenizer);

        while (!stringStack.empty()) {
            resultString.append(" ").append(stringStack.pop());
        }
        return resultString.toString();
    }

    private int getPriority(String token) {

        return operationMap.get(token);

    }

    private boolean isFunction(String token) {

        return Arrays.asList(FUNCTIONS).contains(token);

    }

    private boolean isOperation(String token) {

        return Arrays.asList(operations).contains(token);

    }

    private double calcFunction(String function, double value) {

        return oneArgumentFunctionMap.get(function).calculate(value);
    }

    private double simpleOperationCalc(String function, double firstValue, double secondValue) {

        return doubleArgumentFunctionMap.get(function).calculate(firstValue, secondValue);

    }

    private void fillStack(StringTokenizer stringTokenizer) throws Exception {
        String temp;
        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken();
            if (isOperation(token)) {

                while (!stringStack.empty()) {
                    if (isOperation(stringStack.lastElement()) && (getPriority(token) <= getPriority(stringStack.lastElement()))) {
                        resultString.append(" ").append(stringStack.pop()).append(" ");
                    } else {
                        resultString.append(" ");
                        break;
                    }
                }
                stringStack.push(token);
            } else if (token.equals("(")) {
                stringStack.push(token);
            } else if (token.equals(")")) {
                temp = stringStack.lastElement();
                while (!temp.equals("(")) {
                    if (stringStack.size() < 1) {
                        throw new Exception("Expression error");
                    }
                    resultString.append(" ").append(temp).append(" ");
                    stringStack.remove(temp);
                    temp = stringStack.lastElement();
                }
                stringStack.remove(temp);
                if (stringStack.size() > 0) {
                    if (isFunction(stringStack.lastElement())) {
                        resultString.append(" ").append(stringStack.lastElement()).append(" ");
                        stringStack.remove(stringStack.lastElement());
                    }
                }
            } else if (isFunction(token)) {
                stringStack.push(token);
            } else {
                resultString.append(" ").append(token).append(" ");
            }
        }
    }

    public double calculate(String stringToCalculate) throws Exception {
        double firstParam, secondParam;
        String tempString;
        Deque<Double> stack = new ArrayDeque<>();
        StringTokenizer tokenizer = new StringTokenizer(stringToCalculate);
        while (tokenizer.hasMoreTokens()) {
            try {
                tempString = tokenizer.nextToken().trim();
                if ((1 == tempString.length() && isOperation(tempString)) || isFunction(tempString)) {
                    if (Arrays.asList(FUNCTIONS).contains(tempString)) {
                        firstParam = stack.pop();
                        firstParam = calcFunction(tempString, firstParam);
                        stack.push(firstParam);
                    }
                    if (1 == tempString.length() && isOperation(tempString)) {
                        secondParam = stack.pop();
                        firstParam = stack.pop();
                        firstParam = simpleOperationCalc(tempString, firstParam, secondParam);
                        stack.push(firstParam);
                    }
                } else {
                    firstParam = Double.parseDouble(tempString);
                    stack.push(firstParam);
                }
            } catch (Exception e) {
                throw new Exception("Wrong character in expressions");
            }
        }
        if (stack.size() > 1) {
            throw new Exception("Wrong number of operations");
        }
        return stack.pop();
    }
}
