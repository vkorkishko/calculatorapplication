    package com.griddynamics.intern;

    import com.griddynamics.intern.logic.Calculator;

    import java.io.BufferedReader;
    import java.io.InputStreamReader;

    public class WorkWithCalc {

        public static void main(String[] args) {

            Calculator myCalc = new Calculator();

            BufferedReader d = new BufferedReader(new InputStreamReader(System.in));

            String inputString;
            while (true) {
                try {
                    System.out.println("Input expression");
                    inputString = d.readLine();
                    if(inputString.equals("exit"))
                        System.exit(0);
                    inputString = myCalc.convertString(inputString);
                    System.out.println(myCalc.calculate(inputString));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
