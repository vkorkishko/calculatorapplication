package com.griddynamics.intern.interfaces;

public interface OneArgumentFunction {

    double calculate(double param);
}
