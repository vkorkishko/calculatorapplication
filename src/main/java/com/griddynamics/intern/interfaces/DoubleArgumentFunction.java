package com.griddynamics.intern.interfaces;

public interface DoubleArgumentFunction {

    double calculate(double firstParam, double secondParam);
}
